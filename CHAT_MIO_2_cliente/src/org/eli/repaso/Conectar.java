package org.eli.repaso;

import javax.swing.*;
import java.awt.event.*;

public class Conectar extends JDialog {
    private JPanel panel;
    private JButton btAceptar;
    private JButton btCancelar;
    private JTextField tfHost;
    private Constantes.Accion accion;
    private String host;
    private Chat chat;

    public Conectar() {
        setContentPane(panel);
        pack();
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setModal(true);
        setLocationRelativeTo(null);

        btAceptar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aceptar();
            }
        });

        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelar();
            }
        });
        tfHost.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    btAceptar.doClick();
            }
        });
    }
    private void aceptar() {
        accion = Constantes.Accion.ACEPTAR;

        host = tfHost.getText();
        if (host.equals("")) {
            JOptionPane.showConfirmDialog(null,"Introduce host","Error",JOptionPane.ERROR_MESSAGE);
            return;
        }

        setVisible(false);
    }

    private void cancelar() {
        accion = Constantes.Accion.CANCELAR;
        setVisible(false);
    }

    public String getHost() {
        return host;
    }

    public Constantes.Accion mostrarDialogo() {

        setVisible(true);

        return accion;
    }
}
