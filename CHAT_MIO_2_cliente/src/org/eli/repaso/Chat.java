package org.eli.repaso;

import javax.swing.*;
import javax.swing.text.StringContent;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by Eliseo on 03/06/2016.
 */
public class Chat {
    private JPanel panel1;
    private JTextField tfMensaje;
    private JButton btConectado;
    private JButton btDesconectado;
    private JTextArea taChat;
    private JList lvAmigos;
    private JLabel lbEstado;
    private DefaultListModel modeloListaClientes;

    private Socket socket;
    private PrintWriter salida;
    private BufferedReader entrada;
    private boolean conectado;
    private String nick;
    private int puerto=5555;
    public ArrayList<String> listaUsuariosBloqueados;
    private String sobrenombre;
    //private MensajePrivado mensajePrivado;

    public Chat() {
        btConectado.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Conectado");
            }
        });

        btDesconectado.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Desconectado");
            }
        });

        tfMensaje.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    enviarMensaje();
            }
        });


        listaUsuariosBloqueados = new ArrayList<>();
        modeloListaClientes = new DefaultListModel<>();
        lvAmigos.setModel(modeloListaClientes);


        tfMensaje.requestFocus();
    }





    private void conectar() {

        final Conectar conecta = new Conectar();

        if (conecta.mostrarDialogo() == Constantes.Accion.CANCELAR)
            return;


        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String host = conecta.getHost();
                conectarServidor(host);
            }
        });
    }


    private void conectarServidor(String servidor) {

        try {
            socket = new Socket(servidor, puerto);
            salida = new PrintWriter(socket.getOutputStream(), true);
            entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            conectado = true;
            btConectado.setEnabled(true);
            btConectado.setBackground(Color.green);
            btDesconectado.setEnabled(false);
            lbEstado.setText("Conectado");
            Thread hilo = new Thread(new Runnable() {
                public void run() {
                    while (conectado) {
                        try {
                            if (socket.isClosed()) {
                                conectado = false;
                                JOptionPane.showConfirmDialog(null,"Fatal error","Servidor desconectado",JOptionPane.ERROR_MESSAGE);
                                break;
                            }
                            String mensaje = entrada.readLine();

                            if (mensaje == null)
                                continue;
                            int indice = 0;
                            if (mensaje.startsWith("/server")) {
                                indice = mensaje.indexOf(" ");
                                taChat.append("** " + mensaje.substring(indice + 1) + " **\n");
                            } else if (mensaje.startsWith("/users")) {
                                indice = mensaje.indexOf(" ", 7);
                                nick = mensaje.substring(7, indice);
                                for(int x=0; x< listaUsuariosBloqueados.size(); x++){
                                    if(nick.equals(listaUsuariosBloqueados.get(x))){
                                        taChat.append("Usuario ignorado\n");
                                        break;
                                    }
                                }
                                taChat.append(" " + nick + ": ");
                                taChat.append(mensaje.substring(indice + 1) + "\n");

                            }  else if(mensaje.startsWith("/ignorado")) {
                                String[] palabras = mensaje.split(" ");
                                String amigoIgnorado = palabras[1];
                                for (int i = 0; i < modeloListaClientes.size(); i++) {
                                    if (modeloListaClientes.get(i).equals(amigoIgnorado)) {
                                        taChat.append(amigoIgnorado+" ignorado\n");
                                        modeloListaClientes.set(i, modeloListaClientes.get(i) + " esta bloqueado");
                                        listaUsuariosBloqueados.add(amigoIgnorado);
                                    }
                                }
                            } else if(mensaje.startsWith("/sobrenombre")) {
                                String[] palabras = mensaje.split(" ");
                                nick = palabras[1];
                                sobrenombre = palabras[1];
                            } else if (mensaje.startsWith("/nicks")) {
                                String[] nicks = mensaje.split(",");
                                modeloListaClientes.clear();
                                for (int i = 1; i < nicks.length; i++) {
                                    modeloListaClientes.addElement(nicks[i]);
                                }
                            } else if (mensaje.startsWith("/private")){
                                String[] palabras = mensaje.split(" ");
                                String recibe = palabras[2];
                                recibe = recibe.substring(0, recibe.length() - 1);
                                System.out.print(mensaje);
                                System.out.print("Nick del enviador: "+nick);
                                System.out.print("Recibe: "+recibe);
                                for (int i = 0; i < listaUsuariosBloqueados.size(); i++) {
                                    if (listaUsuariosBloqueados.get(i).equals(palabras[1])) {
                                        taChat.append("El usuario " + palabras[1] + " te ha escrito un mensaje privado pero está ignorado\n");
                                        break;
                                    }
                                }
                                indice = mensaje.indexOf(":");
                                if (recibe.equals(sobrenombre)){
                                    taChat.append(sobrenombre + " mensaje privado de " + palabras[1] + " : \n");
                                    taChat.append("\t"+mensaje.substring(indice+1)+"\n");
                                } else if (palabras[1].equals(sobrenombre)){
                                    taChat.append("Mensaje privado enviado correctamente a "+recibe+"\n");
                                }
                            } else if(mensaje.startsWith("/quit")){
                                desconectar();
                            }


                        } catch (SocketException se) {
                            desconectar();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            });
            hilo.start();

        } catch (UnknownHostException uhe) {
            uhe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }



    private void enviarMensaje() {

        String mensaje = tfMensaje.getText();
        salida.println(mensaje);

        tfMensaje.setText("");
    }

    private void desconectar() {
        try {
            salida.println("/quit");
            taChat.append("Desconectado correctamente");
            conectado = false;
            btConectado.setBackground(new Color(187,187,187));
            btDesconectado.setEnabled(true);
            btDesconectado.setBackground(Color.red);
            btConectado.setEnabled(false);
            lbEstado.setText("Desconectado");
            socket.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void salir() {
        System.exit(0);
    }

    public JMenuBar MenuBar() {

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        menuBar.add(menu);
        JMenuItem item = new JMenuItem("Conectar");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                conectar();
            }
        });
        menu.add(item);
        item = new JMenuItem("Desconectar");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                desconectar();
            }
        });
        menu.add(item);
        item = new JMenuItem("Salir");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                salir();
            }
        });
        menu.add(item);

        return menuBar;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("UOU");
        Chat Chat = new Chat();
        frame.setJMenuBar(Chat.MenuBar());
        frame.setContentPane(Chat.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setSize(600, 600);
    }

}