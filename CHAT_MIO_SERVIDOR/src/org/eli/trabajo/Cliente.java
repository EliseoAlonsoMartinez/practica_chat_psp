package org.eli.trabajo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by Eliseo on 03/06/2016.
 */
public class Cliente extends Thread{
    private Socket socket;
    private PrintWriter salida;
    private BufferedReader entrada;
    private Servidor servidor;
    private String nick;
    private String ip;
    private boolean dobleip=false;

    public Cliente(Socket socket,Servidor servidor)throws IOException{
        this.socket=socket;
        this.servidor=servidor;
        salida=new PrintWriter(socket.getOutputStream(),true);
        entrada=new BufferedReader(new InputStreamReader(socket.getInputStream()));

    }

    public PrintWriter getSalida() {
        return salida;
    }

    public void cambiarNick(String nick) {
        this.nick = nick;
    }

    public String getNick() {
        return nick;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public void run() {
        boolean encontrado = false;
        ip = socket.getInetAddress().getHostName();
        /*for (int i = 0; i < servidor.getIPS().size(); i++) {
            if (servidor.getIPS().get(i).equals(ip)) {
                encontrado = true;
            }
        }*/
        servidor.anadirIP(this);
        /*if (encontrado) {
            salida.println("/server No puedes conectarte desde el mismo equipo");
            salida.println("/quit");
        } else {*/
            salida.println("/server Bienvenido al chat KalamarDeMar" + "(" + socket.getInetAddress().getHostName() + ")");
            salida.println("/server Introduce un Nick y seguido de un espacio el password para registrarse");
            try {
                String mensaje = entrada.readLine();
                String[] palabras = mensaje.split(" ");
                String nick = palabras[0];
                cambiarNick(nick);
                servidor.nuevo_cliente(nick, palabras[1], this);
                do {
                    if (servidor.usuario_registrado() == false) {
                        mensaje = entrada.readLine();
                        palabras = mensaje.split(" ");
                        nick = palabras[0];
                        cambiarNick(nick);
                        servidor.nuevo_cliente(nick, palabras[1], this);
                    }
                } while (servidor.usuario_registrado() == false);
                servidor.anadirCliente(this);
                salida.println("/server Hola " + nick);
                salida.println("/sobrenombre " + nick);
                salida.println("/server Tienes " + (servidor.getNumeroClientes() - 1) + " amigos conectados");
                salida.println("/server Escribe /quit para salir");
                salida.println("/server Escribe '/private' y el nombre del amigo para mandarle un mensaje");
                salida.println("/server Escribe /ignorado y el nombre del amigo para ignorarle");

                if (servidor.usuario_repetido() == true) {
                    salida.println("/server Error, no puedes realizar dos conexiones desde el mismo equipo");
                    salida.println("/quit");
                    socket.close();
                }
                servidor.enviarNicks(this);

                String linea = null;
                while ((linea = entrada.readLine()) != null) {

                    if (linea.equals("/quit")) {
                        salida.println("/server Desconectado");
                        socket.close();
                        servidor.eliminarCliente(this);
                        break;
                    }

                    if (linea.startsWith("/ignorado")) {
                        String[] comandos = linea.split(" ");
                        System.out.print(comandos[1] + " ignorado");
                        salida.println("/ignorado " + comandos[1]);
                    } else if (linea.startsWith("/private")) {
                        String[] comandos = linea.split(" ");
                        int cantidaduno = comandos[0].length();
                        int cantidaddos = nick.length();
                        int cantidadtres = comandos[1].length();
                        int cantidadtotal = cantidaduno + cantidaddos + cantidadtres - 2;
                        String indice = linea.substring(cantidadtotal);
                        System.out.println("/private " + nick + " " + comandos[1] + ": " + indice);
                        servidor.enviarATodos("/private " + nick + " " + comandos[1] + ": " + indice);
                    } else {
                        servidor.enviarATodos("/users " + nick + " " + linea);
                    }

                }

            } catch (SocketException se) {
                se.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

    //}
}
