package org.eli.trabajo;

import java.io.IOException;

/**
 * Created by Eliseo on 03/06/2016.
 */
public class Main {

    public static final int PUERTO = 5555;

    public static void main(String args[]) {

        Servidor servidor = new Servidor(PUERTO);
        Cliente cliente = null;

        try {
            servidor.conectar();

            while (servidor.estaConectado()) {
                cliente = new Cliente(servidor.escuchar(), servidor);
                System.out.println("Nuevo cliente conectado");
                cliente.start();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
