package org.eli.trabajo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Eliseo on 03/06/2016.
 */
public class Servidor {
    private ServerSocket socket;
    private int puerto;
    private ArrayList<Cliente> clientes;
    private ArrayList<String> IPS;
    private ArrayList<Usuario> usuarios;
    private boolean usuarioRegistrado;
    private boolean usuarioRepetido;

    public Servidor(int puerto){
        this.puerto=puerto;
        clientes =new ArrayList<>();
        IPS=new ArrayList<>();
        usuarios=new ArrayList<>();
    }

    public void anadirCliente(Cliente cliente){
        try {


            for (int i = 0; i < clientes.size(); i++) {
                if (clientes.get(i).getNick().equals(cliente.getNick())) {
                    return;
                }
            }
            clientes.add(cliente);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean estaConectado() {
        return !socket.isClosed();
    }

    public void eliminarCliente(Cliente cliente) {
        clientes.remove(cliente);
    }

    public Socket escuchar() throws IOException {
        return socket.accept();
    }

    public void conectar() throws IOException {
        socket = new ServerSocket(puerto);
    }

    public int getNumeroClientes() {
        return clientes.size();
    }

    public String obtenerNicks() {

        String nicks = "/nicks,";
        for (Cliente cliente : clientes)
            nicks += cliente.getNick() + ",";

        return nicks;
    }

    public void enviarATodos(String mensaje) {
        for (Cliente cliente : clientes)
            cliente.getSalida().println(mensaje);
    }

    public void enviarNicks(Cliente cliente1) {
        for (Cliente cliente : clientes) {
            cliente.getSalida().println(obtenerNicks());
        }
        cliente1.getSalida().println(obtenerNicks());
    }
    public void anadirIP(Cliente cliente){
        IPS.add(cliente.getIp());
    }

    public ArrayList<String> getIPS() {
        return IPS;
    }

    public void nuevo_cliente(String nick, String password, Cliente cliente){
        Usuario usuario = new Usuario();
        if(usuarios.size()==0){
            usuario.setNombre(nick);
            usuario.setContrasena(password);
            usuarios.add(usuario);
            cliente.getSalida().println("/server Nuevo usuario");
            usuarioRegistrado =true;
            usuarioRepetido =false;
            return;
        }
        for(int i=0; i<usuarios.size(); i++) {
            if (usuarios.get(i).getNombre().equals(nick)) {
                if (usuarios.get(i).getContrasena().equals(password)) {
                    cliente.getSalida().println("/server Tu nick es: " + nick);
                    usuarioRegistrado = true;
                    usuarioRepetido =true;
                    return;
                } else {
                    cliente.getSalida().println("/server La contraseņa no coincide");
                    usuarioRegistrado = false;
                    return;
                }
            }
        }
        usuario.setNombre(nick);
        usuario.setContrasena(password);
        usuarios.add(usuario);
        cliente.getSalida().println("/server Registro realizadoa");
        usuarioRegistrado =true;
        usuarioRepetido =false;
        return;

    }


    public boolean usuario_registrado() {
        return usuarioRegistrado;
    }

    public boolean usuario_repetido() {
        return usuarioRepetido;
    }

}
